#!/bin/bash

# Ensure jq is installed
if ! command -v jq &>/dev/null; then
    echo "Error: jq is not installed. Please install it first."
    exit 1
fi

# Get open windows (title, class, and address)
windows_raw=$(hyprctl clients -j | jq -r '.[] | "\(.title) | \(.class) | \(.address)"')

# Exit if no windows are found
[ -z "$windows_raw" ] && exit

# Define icons and simple names
declare -A icons
icons=(
    ["firefox"]="🌍 Firefox"
    ["alacritty"]=" Alacritty"
    ["code"]=" VS Code"
    ["thunar"]=" Files"
    ["discord"]=" Discord"
    ["org.keepassxc.KeePassXC"]="🔑 KeePassXC"
)

# Generate a formatted list while keeping a mapping
declare -A title_map
formatted_list=""

while IFS= read -r line; do
    title=$(echo "$line" | cut -d'|' -f1 | xargs)
    class=$(echo "$line" | cut -d'|' -f2 | xargs)
    address=$(echo "$line" | cut -d'|' -f3 | xargs)

    display_name="${icons[$class]:-$class}"  # Use icon if available, else use class name
    formatted_entry="$display_name | $title"

    title_map["$formatted_entry"]="$title"
    formatted_list+="$formatted_entry"$'\n'
done <<< "$windows_raw"

# Show window switcher (Tofi)
selected=$(echo -e "$formatted_list" | tofi)

# Extract original title from selection
if [ -n "$selected" ]; then
    original_title="${title_map[$selected]}"

    # Find the matching window address
    window_address=$(hyprctl clients -j | jq -r --arg sel "$original_title" '.[] | select(.title == $sel) | .address')

    if [ -n "$window_address" ]; then
        hyprctl dispatch focuswindow address:$window_address
    else
        echo "Error: Could not find window address for '$original_title'"
    fi
fi

