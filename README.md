
# dotfiles

This is my dotfiles for my hyprland config. I manage all my dotfiles with [gnu stow](https://www.gnu.org/software/stow/). Here is the [documentation](https://www.gnu.org/software/stow/manual/stow.html) to show you how to use the software. The main idea is to start a file that I call ```dotfiles``` and it is located in my $HOME directory. Once inside my ```dotfiles``` directory make sure that it is layed out just like my acual $HOME directory.

Once I have cloned the dotfiles repository, you will need to ```cd``` into the ```dotfiles``` directory and run the following:
```bash
stow .
```
When the above command is ran it will automaticlly link all the ```dotfiles``` to the correct paths.

## Quick links
Here are all my quick links to all my dotfiles documentation:
* [alacritty](https://alacritty.org/config-alacritty.html)
* [htop](https://www.man7.org/linux/man-pages/man1/htop.1.html)
* [hyprland](https://wiki.hyprland.org/)
* [neofetch](https://github.com/dylanaraps/neofetch/wiki)
* [neovim](https://neovim.io/doc/user/)
* [ohmyposh](https://ohmyposh.dev/docs)
* [tmux](http://man.openbsd.org/OpenBSD-current/man1/tmux.1)
* [tofi](https://github.com/philj56/tofi)
* [vim](https://vimdoc.sourceforge.net/htmldoc/usr_toc.html)
* [waybar](https://github.com/Alexays/Waybar/wiki)
* [zsh](https://zsh.sourceforge.io/Doc/Release/zsh_toc.html)

---

### Simple Git Commands 
Here are my basic edc commands for git:
* ```git add -u```
    ```bash
    gaa
    ```
* ```git status```
    ```bash
    gst
    ```
* ```git commit -m "example"```
    ```bash
    gcmsg "example"
    ```
* ```git push```
    ```bash
    gp
    ```
