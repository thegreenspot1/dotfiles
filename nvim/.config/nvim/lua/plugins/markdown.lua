--   ________                              _________              __     
--  /  _____/______   ____   ____   ____  /   _____/_____   _____/  |_   
-- /   \  __\_  __ \_/ __ \_/ __ \ /    \ \_____  \\____ \ /  _ \   __\  
-- \    \_\  \  | \/\  ___/\  ___/|   |  \/        \  |_> >  <_> )  |    
--  \______  /__|    \___  >\___  >___|  /_______  /   __/ \____/|__|    
--         \/            \/     \/     \/        \/|__|                  
-- ···············································································
-- 
-- ···············································································

return{
    'MeanderingProgrammer/render-markdown.nvim',
    dependencies = { 'nvim-treesitter/nvim-treesitter', 'echasnovski/mini.nvim' }, -- if you use the mini.nvim suite
    -- dependencies = { 'nvim-treesitter/nvim-treesitter', 'echasnovski/mini.icons' }, -- if you use standalone mini plugins
    -- dependencies = { 'nvim-treesitter/nvim-treesitter', 'nvim-tree/nvim-web-devicons' }, -- if you prefer nvim-web-devicons
    ---@module 'render-markdown'
    ---@type render.md.UserConfig
    opts = {},
    config = function()
        -- Enable folding based on treesitter
        vim.opt.foldmethod = "expr"
        vim.opt.foldexpr = "nvim_treesitter#foldexpr()"
        vim.opt.foldlevel = 99 -- Keep all folds open by default
        
        -- Custom keybindings
        vim.keymap.set('n', '<leader>fc', 'zc', { desc = "Close fold" })
        vim.keymap.set('n', '<leader>fo', 'zo', { desc = "Open fold" })
        vim.keymap.set('n', '<leader>fa', 'zM', { desc = "Close all folds" })
        vim.keymap.set('n', '<leader>fr', 'zR', { desc = "Open all folds" })
    end
}
