#!/usr/bin/env python3

import os
import subprocess

# Define custom border settings
#RED_BORDER = "0xffcc0000"  # Red color
BORDER_SIZE = "1"          # 2px thick

# Store original settings
#ORIGINAL_BORDER_COLOR = "rgb(136,192,208)"  # Change to your actual color
ORIGINAL_BORDER_SIZE = "10"            # Change to your normal border size

# Get Hypridle status
def get_hypridle_status():
    result = subprocess.run(["pgrep", "-x", "hypridle"], stdout=subprocess.PIPE)
    return result.returncode == 0  # True if running, False if not

# Toggle Hypridle and update borders
def toggle_hypridle():
    if get_hypridle_status():
        os.system("pkill hypridle")  # Stop Hypridle
        #os.system(f"hyprctl keyword general:col.active_border {RED_BORDER}")
        os.system(f"hyprctl keyword general:border_size {BORDER_SIZE}")
    else:
        os.system("hypridle &")  # Start Hypridle
        #os.system(f"hyprctl keyword general:col.active_border {ORIGINAL_BORDER_COLOR}")
        os.system(f"hyprctl keyword general:border_size {ORIGINAL_BORDER_SIZE}")

toggle_hypridle()
