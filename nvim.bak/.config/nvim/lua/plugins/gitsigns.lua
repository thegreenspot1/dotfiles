--   ________                              _________              __     
--  /  _____/______   ____   ____   ____  /   _____/_____   _____/  |_   
-- /   \  __\_  __ \_/ __ \_/ __ \ /    \ \_____  \\____ \ /  _ \   __\  
-- \    \_\  \  | \/\  ___/\  ___/|   |  \/        \  |_> >  <_> )  |    
--  \______  /__|    \___  >\___  >___|  /_______  /   __/ \____/|__|    
--         \/            \/     \/     \/        \/|__|                  
-- ···············································································
-- 
-- ···············································································
-- Adds git related signs to the gutter, as well as utilities for managing changes

return {
  'lewis6991/gitsigns.nvim',
  opts = {
    -- See `:help gitsigns.txt`
    signs = {
      add = { text = '+' },
      change = { text = '~' },
      delete = { text = '_' },
      topdelete = { text = '‾' },
      changedelete = { text = '~' },
    },
    signs_staged = {
      add = { text = '+' },
      change = { text = '~' },
      delete = { text = '_' },
      topdelete = { text = '‾' },
      changedelete = { text = '~' },
    },
  },
}
