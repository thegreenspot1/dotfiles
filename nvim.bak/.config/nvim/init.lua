--   ________                              _________              __     
--  /  _____/______   ____   ____   ____  /   _____/_____   _____/  |_   
-- /   \  __\_  __ \_/ __ \_/ __ \ /    \ \_____  \\____ \ /  _ \   __\  
-- \    \_\  \  | \/\  ___/\  ___/|   |  \/        \  |_> >  <_> )  |    
--  \______  /__|    \___  >\___  >___|  /_______  /   __/ \____/|__|    
--         \/            \/     \/     \/        \/|__|                  
-- ···············································································
-- https://neovim.io/doc/ 
-- ···············································································

require 'core.options'
require 'core.keymaps'

local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
if not (vim.uv or vim.loop).fs_stat(lazypath) then
  local lazyrepo = 'https://github.com/folke/lazy.nvim.git'
  local out = vim.fn.system { 'git', 'clone', '--filter=blob:none', '--branch=stable', lazyrepo, lazypath }
  if vim.v.shell_error ~= 0 then
    error('Error cloning lazy.nvim:\n' .. out)
  end
end
vim.opt.rtp:prepend(lazypath)
-- TODO here
-- NOTE: Here is where you install your plugins.
require('lazy').setup({
    require 'plugins.neotree',
--    require 'plugins.colortheme',
    require 'plugins.rose-pine',
    require 'plugins.bufferline',
    require 'plugins.lualine',
    require 'plugins.treesitter',
    require 'plugins.telescope',
    require 'plugins.lsp',
    require 'plugins.autocomp',
    require 'plugins.gitsigns',
    require 'plugins.alpha',
    require 'plugins.indent-blanklines',
    require 'plugins.misc',
    require 'plugins.markdown'
})
